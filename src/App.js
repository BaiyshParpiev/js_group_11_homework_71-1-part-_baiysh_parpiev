import React from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Orders from "./containers/Orders/Orders";
import Dishes from "./containers/Dishes/Dishes";
import Layout from "./components/Layout/Layout";
import ChangingDishes from "./containers/Dishes/ChangingDishes/ChangingDishes";

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={Dishes}/>
        <Route path="/dishes" exact component={Dishes}/>
        <Route path='/orders' component={Orders}/>
          <Route path='/changingDishes' component={ChangingDishes}/>
          <Route path='/edit/:id' component={ChangingDishes}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
);

export default App;

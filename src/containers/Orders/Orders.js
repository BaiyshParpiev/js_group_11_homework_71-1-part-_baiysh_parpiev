import React, {useEffect} from 'react';
import './Orders.css';
import Button from "../../components/UI/Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {fetchOrders} from "../../store/actions/ordersActions";

const Orders = () => {
    const dispatch = useDispatch();
    const orders = useSelector(state => state.orders.orders);
    const dishes = useSelector(state => state.dishes.dishes)

    useEffect(() => {
        dispatch(fetchOrders());
    }, [dispatch]);

    let order = () => {
        Object.keys(orders).map(o => {
            let price = 0;
            const simple = Object.key(orders[o]).map(order => {
                price += dishes[order].price;
                return (<>
                        <div className='row'>
                            <h3><span>Number x {orders[o].order}</span>{dishes[order].name}</h3>
                            <h5>Price</h5>
                        </div>
                        <div className="row">
                            <h3><span>Number x</span>Name of food</h3>
                            <h5>Price</h5>
                        </div>
                    </>
                )
            });
            return (
                <div className="column">
                    {simple}
                    <div className='margin'>
                        <h4>Order Total</h4>
                        <h5>Price: {price}</h5>
                        <Button name='Complete Order'/>
                    </div>
                </div>)
        })
    }

    return (
        <div className='container'>
            <h4>Orders</h4>
            <div className="column">
                <div className="OrderItem">
                    <div className="row ">

                    </div>
                </div>
            </div>
        </div>
    );
};

export default Orders;
import React, {useEffect, useState} from 'react';
import Button from "../../../components/UI/Button/Button";
import './ChangingDishes.css';
import {useDispatch, useSelector} from "react-redux";
import {changeDish, createDish} from "../../../store/actions/dishesActions";
import {useHistory} from "react-router-dom";
import Spinner from "../../../components/UI/Spinner/Spinner";

const ChangingDishes = ({match}) => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);
    const loading = useSelector(state => state.dishes.loading);
    const error = useSelector(state => state.dishes.error);
    const history = useHistory();
    const [info, setInfo] = useState({
        img: '',
        name: '',
        price: '',
    });

    useEffect(() => {
        if(match.params.id){
            const d = match.params.id;
            setInfo(prev => ({
                ...prev,
                img: dishes[d].img,
                name: dishes[d].name,
                price: dishes[d].price,
            }))
        }
    }, [match.params.id, dishes])


    const onChange = e => {
        const {name, value} = e.target;
        setInfo(prev => ({
            ...prev,
            [name]: value,
        }));
    }

    const onHandlerClick = e => {
        e.preventDefault();
        dispatch(createDish(info));
        history.replace('/orders')
    }

    const onHandlerEdit = e => {
        e.preventDefault();
        dispatch(changeDish(match.params.id, info));
        history.replace('/orders')
    }

    let edit = (
        <form onSubmit={match.params.id ? onHandlerEdit : onHandlerClick} className='ChangingDishes'>
            <div><input type="text" value={info.img} name='img' onChange={onChange} placeholder='Src to img:'/></div>
            <div><input type="text" value={info.name} name='name' onChange={onChange} placeholder='Name of Dish: '/></div>
            <div><input type="text" value={info.price} name='price' onChange={onChange} placeholder='Price: '/></div>
            <div><Button type="submit" name='Save'/></div>
        </form>
    );

    if(loading){
        edit = <Spinner/>
    }else if(error){
        edit = <div>Sorry something went wrong</div>
    }



    return edit;
};

export default ChangingDishes;
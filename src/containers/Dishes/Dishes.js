import React, {useEffect} from 'react';
import './Dishes.css';
import Button from "../../components/UI/Button/Button";
import DishesItem from "../../components/DishesItem/DishesItem";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {fetchDishes, removeDish} from "../../store/actions/dishesActions";
import Spinner from "../../components/UI/Spinner/Spinner";

const Dishes = ({history, match}) => {
    const dispatch = useDispatch();
    const {dishes, fetchLoading, error} = useSelector(state => ({
        dishes: state.dishes.dishes,
        fetchLoading: state.dishes.fetchLoading,
        error: state.dishes.error,
    }), shallowEqual)

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch, match.path])

    const onHandlerClick = () => {
        history.replace('/changingDishes')
    }

    const onClickEdit = e => {
        history.replace('/edit/' + e);
    }

    const onRemove = e => {
        dispatch(removeDish(e));
        history.replace('/orders')
    }

    let dishesComponents = <>
        <div className='container'>
            <div className="row">
                <h2>Dishes</h2>
                <Button name='+ Add new Dishes' onclick={onHandlerClick}/>
            </div>
            <div className="column">
                {dishes && Object.keys(dishes).map(d => {
                    return <DishesItem
                        key={d}
                        dishes={dishes[d]}
                        remove={() => onRemove(d)}
                        edit={() => onClickEdit(d)}
                    />})}
            </div>
        </div>
    </>;

    if(fetchLoading){
        dishesComponents = <Spinner/>
    }else if(error){
        dishesComponents = <div>Sorry something went wrong</div>
    }


    return  dishesComponents
};

export default Dishes;
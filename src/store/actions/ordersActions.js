import {axiosApi} from "../../axiosApi";
export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';

export const REMOVE_ORDER_REQUEST = 'REMOVE_ORDER_REQUEST';
export const REMOVE_ORDER_SUCCESS = 'REMOVE_ORDER_SUCCESS';
export const REMOVE_ORDER_FAILURE = 'REMOVE_ORDER_FAILURE';

export const removeOrderRequest = () => ({type: REMOVE_ORDER_REQUEST});
export const removeOrderSuccess = () => ({type: REMOVE_ORDER_SUCCESS});
export const removeOrderFailure = error => ({type: REMOVE_ORDER_FAILURE, payload: error});

export const fetchOrdersRequest = () => ({type: FETCH_ORDERS_REQUEST});
export const fetchOrdersSuccess = dishes => ({type: FETCH_ORDERS_SUCCESS, payload: dishes});
export const fetchOrdersFailure = error => ({type: FETCH_ORDERS_FAILURE, payload: error});


export const fetchOrders = () => {
    return async dispatch => {
        try{
            dispatch(fetchOrdersRequest());
            const response = await axiosApi.get('/orders.json');
            dispatch(fetchOrdersSuccess(response.data));
        }catch(error) {
            dispatch(fetchOrdersFailure(error));
            throw error;
        }
    }
};

export const removeDish= (id) => {
    return async dispatch => {
        try{
            dispatch(removeOrderRequest());
            await axiosApi.delete(`/orders/${id}.json`);
            dispatch(removeOrderSuccess());
        }catch(error) {
            dispatch(removeOrderFailure(error));
            throw error;
        }
    }
};
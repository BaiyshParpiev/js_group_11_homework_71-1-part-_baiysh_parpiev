import {axiosApi} from "../../axiosApi";

export const DISHES_REQUEST = 'ORDER_REQUEST';
export const DISHES_SUCCESS = 'ORDER_SUCCESS';
export const DISHES_FAILURE = 'ORDER_FAILURE';

export const CHANGE_DISHES_REQUEST = 'CHANGE_ORDERS_REQUEST';
export const CHANGE_DISHES_SUCCESS = 'CHANGE_ORDERS_SUCCESS';
export const CHANGE_DISHES_FAILURE = 'CHANGE_ORDERS_FAILURE';


export const FETCH_DISHES_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_DISHES_FAILURE = 'FETCH_ORDERS_FAILURE';

export const REMOVE_REQUEST = 'REMOVE_REQUEST';
export const REMOVE_SUCCESS = 'REMOVE_SUCCESS';
export const REMOVE_FAILURE = 'REMOVE_FAILURE';


export const dishesRequest = () => ({type: DISHES_REQUEST});
export const dishesSuccess = () => ({type: DISHES_SUCCESS});
export const dishesFailure = error => ({type: DISHES_FAILURE, payload: error});

export const removeRequest = () => ({type: REMOVE_REQUEST});
export const removeSuccess = () => ({type: REMOVE_SUCCESS});
export const removeFailure = error => ({type: REMOVE_FAILURE, payload: error});

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, payload: dishes});
export const fetchDishesFailure = error => ({type: FETCH_DISHES_FAILURE, payload: error});

export const changeDishesRequest = () => ({type: CHANGE_DISHES_REQUEST});
export const changeDishesSuccess = dishes => ({type: CHANGE_DISHES_SUCCESS});
export const changeDishesFailure = error => ({type: CHANGE_DISHES_FAILURE, payload: error});

export const fetchDishes = () => {
    return async dispatch => {
        try{
            dispatch(fetchDishesRequest());
            const response = await axiosApi.get('/dishes.json');
            dispatch(fetchDishesSuccess(response.data));
        }catch(error) {
            dispatch(fetchDishesFailure(error));
            throw error;
        }
    }
}

export const createDish = dishData => {
    return async dispatch => {
        try{
            dispatch(dishesRequest());
            await axiosApi.post('/dishes.json', dishData);
            dispatch(dishesSuccess());
        }catch(error) {
            dispatch(dishesFailure(error));
            throw error;
        }
    }
}

export const changeDish = (id, dishData) => {
    return async dispatch => {
        try{
            dispatch(changeDishesRequest());
            await axiosApi.put(`/dishes/${id}.json`, dishData);
            dispatch(changeDishesSuccess());
        }catch(error) {
            dispatch(changeDishesFailure(error));
            throw error;
        }
    }
}

export const removeDish= (id) => {
    return async dispatch => {
        try{
            dispatch(removeRequest());
            await axiosApi.delete(`/dishes/${id}.json`);
            dispatch(removeSuccess());
        }catch(error) {
            dispatch(removeFailure(error));
            throw error;
        }
    }
}



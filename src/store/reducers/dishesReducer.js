import {
    CHANGE_DISHES_FAILURE,
    CHANGE_DISHES_REQUEST, CHANGE_DISHES_SUCCESS, DISHES_FAILURE, DISHES_REQUEST, DISHES_SUCCESS,
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS, REMOVE_FAILURE, REMOVE_REQUEST, REMOVE_SUCCESS
} from "../actions/dishesActions";

const initialState = {
    dishes: null,
    loading: false,
    fetchLoading: false,
    error: null,
}

export const dishesReducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_DISHES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, fetchLoading: false, dishes: action.payload};
        case FETCH_DISHES_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case CHANGE_DISHES_REQUEST:
            return {...state, loading: true};
        case CHANGE_DISHES_SUCCESS:
            return {...state, loading: false,};
        case CHANGE_DISHES_FAILURE:
            return {...state, loading: false, error: action.payload};
        case DISHES_REQUEST:
            return {...state, loading: true};
        case DISHES_SUCCESS:
            return {...state, loading: false,};
        case DISHES_FAILURE:
            return {...state, loading: false, error: action.payload};
        case REMOVE_REQUEST:
            return {...state, loading: true};
        case REMOVE_SUCCESS:
            return {...state, loading: false,};
        case REMOVE_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}
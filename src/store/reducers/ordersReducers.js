import {
    FETCH_ORDERS_FAILURE,
    FETCH_ORDERS_REQUEST,
    FETCH_ORDERS_SUCCESS, REMOVE_ORDER_FAILURE,
    REMOVE_ORDER_REQUEST, REMOVE_ORDER_SUCCESS
} from "../actions/ordersActions";

const initialState = {
    orders: null,
    loading: false,
    fetchLoading: false,
}

export const ordersReducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_ORDERS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ORDERS_SUCCESS:
            return {...state, fetchLoading: false, orders: action.payload};
        case FETCH_ORDERS_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        case REMOVE_ORDER_REQUEST:
            return {...state, loading: true};
        case REMOVE_ORDER_SUCCESS:
            return {...state, loading: false,};
        case REMOVE_ORDER_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}

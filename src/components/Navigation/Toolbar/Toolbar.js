import React from 'react';
import './Toolbar.css';
import NavigationItems from "../NavigationItems/NavigationItems";
import NavigationItem from "../NavigationItem/NavigationItem";

const Toolbar = () => {
    return (
        <header className='Toolbar'>
            <div className="Toolbar-Logo">
                <NavigationItem to='/' exact>Turtle Pizza Admin</NavigationItem>
            </div>
            <nav><NavigationItems/></nav>
        </header>
    );
};

export default Toolbar;
import React from 'react';
import Button from "../UI/Button/Button";
import './DishesItem.css';

const DishesItem = ({dishes, edit, remove}) => {
    return (
        <div className="DishesItem">
            <img src={dishes.img} alt="Name of dishes"/>
            <h5>Name: {dishes.name}</h5>
            <h4>Price: {dishes.price}KGZ</h4>
            <Button name='Edit' onclick={edit}/>
            <Button name='Delete' onclick={remove}/>
        </div>
    );
};

export default DishesItem;